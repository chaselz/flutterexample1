import 'package:flutter/material.dart';

class Alert extends StatefulWidget{
  @override
  _AlertState createState() =>_AlertState();
}
class _AlertState extends State<Alert> {
  OverlayEntry _overlayEntry;
  int current = 0;
  final buttonRowKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Row(
              key: buttonRowKey,
              children: List.generate(
                3,
                    (i) =>
                    RaisedButton(
                      onPressed: () {
                        final index = i + 1;
                        if (current == index) {
                          _overlayEntry?.remove();
                          _overlayEntry = null;
                          current = 0;
                          return;
                        }
                        current = index;
                        _overlayEntry?.remove();
                        final box = buttonRowKey.currentContext
                            .findRenderObject() as RenderBox;
                        final dy = box
                            .localToGlobal(Offset(0, box.size.height))
                            .dy;
                        _overlayEntry = OverlayEntry(
                            builder: (context) =>
                                Positioned.fill(
                                  top: dy,
                                  child: Material(
                                    color: Colors.transparent,
                                    child: Column(
                                      children: [
                                        Container(
                                          color: Theme
                                              .of(context)
                                              .backgroundColor,
                                          alignment: Alignment.center,
                                          constraints: BoxConstraints.expand(
                                              height: 500),
                                          child: Text("menu$index"),
                                        ),
                                        Expanded(
                                            child: GestureDetector(onTap: () {
                                              _overlayEntry.remove();
                                              _overlayEntry = null;
                                              current = 0;
                                            })),
                                      ],
                                    ),
                                  ),
                                ));
                        Overlay.of(context).insert(_overlayEntry);
                      },
                      child: Text("menu${i + 1}"),
                    ),
              )),
        ],
      ),
    );
  }

  showAlertDialog(BuildContext context) {
    // set up the button
    Widget okButton = FlatButton(
      child: Text("認賠回家睡覺"),
      onPressed: () {
        Navigator.of(context).pop(); // 關閉對話框
      },
    );
    Widget allIn = FlatButton(
      child: Text("All In拼下去"),
      onPressed: null,
    );

    Widget fly = FlatButton(
      child: Text("加入空軍"),
      onPressed: null,
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("我的股票"),
      content: Text("虧錢之後該怎麼做？"),
      actions: [
        okButton,
        allIn,
        fly,
      ],
    );

    // show the dialog
    /* showDialog(
    context: context,
    barrierDismissible: false,  //點擊對話旁的遮罩是否關閉
    barrierColor: Colors.black,   //  遮罩顏色
    builder: (BuildContext context) {
      return alert;
    },
  );*/
  }
}

