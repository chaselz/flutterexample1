import 'package:flutter/material.dart';
import 'dart:math' as math;
import 'dart:async';
import 'package:flutter_screenutil/flutter_screenutil.dart';

final Gradient gradient = new LinearGradient(
  colors: <Color>[
    Colors.greenAccent.withOpacity(1.0),
    Colors.yellowAccent.withOpacity(1.0),
    Colors.redAccent.withOpacity(1.0),
  ],
  stops: [
    0.0,
    0.5,
    1.0,
  ],
);

class Circular_arc extends StatefulWidget {
  const Circular_arc({
    Key key,
  }) : super(key: key);

  @override
  _Circular_arcState createState() => _Circular_arcState();
}

class _Circular_arcState extends State<Circular_arc>
    with SingleTickerProviderStateMixin {
  Animation<double> animation;
  AnimationController animController;

  Timer _timer;
  int seconds;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    animController =
        AnimationController(duration: Duration(seconds: 10), vsync: this);

    final curvedAnimation =
        CurvedAnimation(parent: animController, curve: Curves.easeInOutCubic);

    animation = Tween<double>(begin: 0.0, end: 3.14).animate(curvedAnimation)
      ..addListener(() {
        setState(() {});
      });
    animController.repeat(reverse: false); //反轉

    //獲取當期時間
    var now = DateTime.now();
    //獲取 2 分鐘的時間間隔
    var twoHours = now.add(Duration(seconds: 0)).difference(now);
    //獲取總秒數，2 分鐘為 120 秒
    seconds = twoHours.inSeconds;
    startTimer();
  }

  void startTimer() {
    //設定 1 秒回撥一次
    const period = const Duration(seconds: 1);
    _timer = Timer.periodic(period, (timer) {
      //更新介面
      setState(() {
        //秒數加一，因為一秒回撥一次
        seconds++;
      });
      if (seconds == 10) {
        //倒數計時秒數為0，取消定時器
        cancelTimer();
        startTimer();
      }
    });
  }

  void cancelTimer() {
    if (_timer != null) {
      _timer.cancel();
      _timer = null;
      seconds = 0;
    }
  }

//時間格式化，根據總秒數轉換為對應的 hh:mm:ss 格式
  String constructTime(int seconds) {
    //int hour = seconds ~/ 3600;
    int minute = seconds % 3600 ~/ 60;
    int second = seconds % 60;
    return /*formatTime(hour) + ":" + */ formatTime(minute) +
        ":" +
        formatTime(second);
  }

  //數字格式化，將 0~9 的時間轉換為 00~09
  String formatTime(int timeNum) {
    return timeNum < 10 ? "0" + timeNum.toString() : timeNum.toString();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 500.h,
      width: 375.w,
      child: Stack(
        clipBehavior: Clip.none,
        children: [
          Positioned(
            top: 160.h,
            left: 36.w,
            child: CustomPaint(
              size: Size(375.w, 375.h),
              painter: ProgressArc(null, Colors.grey, true, 3.r),
            ),),
          Positioned(
            top: 160.h,
            left: 36.w,
            child: CustomPaint(
              size: Size(375.w, 375.h),
              painter: ProgressArc(animation.value, Colors.black, false, 6.r),
            ),
          ),
          Container(
            alignment: Alignment.bottomCenter,
            child: Text(constructTime(seconds),
              style: TextStyle(color: Colors.black, fontSize: 18.sp)),),

        ],
      ),
    );
  }
}

class ProgressArc extends CustomPainter {
  bool isBackground;
  double arc;
  Color progressColor;
  double lineWidth;

  ProgressArc(this.arc, this.progressColor, this.isBackground, this.lineWidth);

  @override
  void paint(Canvas canvas, Size size) {
    final rect = Rect.fromLTRB(0, 0, 300.w, 300.h);
    final startAngle = -(math.pi / 180) * 190; //計算後變成度數   角度
    final sweepAngle =
        arc != null ? -arc * (160 / 180) : -(math.pi / 180) * 160; //弧度
    final userCenter = false;
    final paint = Paint()
      ..strokeCap = StrokeCap.round //线条结束时的绘制样式
      ..color = progressColor
      ..style = PaintingStyle.stroke
      ..strokeWidth = lineWidth; //線段寬度
    canvas.drawArc(rect, startAngle, sweepAngle, userCenter, paint);
    final paintCircle = Paint()
      ..strokeCap = StrokeCap.round //线条结束时的绘制样式
      ..color = Colors.white
      ..style = PaintingStyle.fill
      ..strokeWidth = lineWidth; //線段寬度

    if (!isBackground) {
      //   paint.shader = gradient.createShader(rect); //漸變著色器
      canvas.drawCircle(
          Offset(
              //外圈
              150.w -
                  150.w *
                      math.cos(
                          (-sweepAngle * (180 / math.pi) + 10) * math.pi / 180),
              150.h +
                  150.h *
                      math.sin((-sweepAngle * (180 / math.pi) + 10) *
                          math.pi /
                          180)),
          7,
          paint);
      canvas.drawCircle(
          Offset(
              //內圈
              150.w -
                  150.w *
                      math.cos(
                          (-sweepAngle * (180 / math.pi) + 10) * math.pi / 180),
              150.h +
                  150.h *
                      math.sin((-sweepAngle * (180 / math.pi) + 10) *
                          math.pi /
                          180)),
          5,
          paintCircle);
    }
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    // TODO: implement shouldRepaint
    return true;
  }
}
