import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil_init.dart';
import 'package:test123/pages/Tabs.dart';

//import 'reg/listData.dart';
void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  int _curentIndex = 0;

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: Size(375, 900),
      allowFontScaling: false,
      builder: () => MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Tabs(),
      ),
    );
  }
}
