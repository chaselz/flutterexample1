import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:test123/pages/tabs/CategoryPages.dart';
import 'package:test123/pages/tabs/HomePages.dart';
import 'package:test123/pages/tabs/SettingsPages.dart';
import 'package:test123/pages/tabs/StockLossPages.dart';

class Tabs extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _TabsState();
  }
}

class _TabsState extends State<Tabs> {
  int _currentIndex = 0;
  var appBarTitles = ['首頁', '練習1', '設定', '=='];
  List _listPageData = [
    //頁面的連結串列
    HomePages(),
    CategoryPages(),
    SettingsPages(),
    StockLossPages(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: this._listPageData[_currentIndex],
      bottomNavigationBar: this._currentIndex !=1 ?BottomNavigationBar(
        currentIndex: this._currentIndex,
        //設定對應的索引值選中
        onTap: (int index) {
          //index 表示選擇選項
          setState(() {
            print("Tagwjlis index = $index");
            this._currentIndex = index;
          });
        },

        iconSize: 36.0,
        //icon的大小
        fixedColor: Colors.red,
        //選中顏色
        type: BottomNavigationBarType.fixed,
        //設定底部tabs可以有多個按鈕
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text("首頁"),
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.category), title: Text("練習1")),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            //title: Text("設定")
            title: new Container(),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            title: Text("=="),
          ),
        ],
      ):null,
      //appBar: this._currentIndex!=2?AppBar(title: Text(appBarTitles[this._currentIndex])):null,
      appBar: abcBar(
        this._currentIndex,
        appBarTitles,
      ),
    );
  }
}

abcBar(index,appBarTitles) {
  print("第$index頁");
  if (index != 2  && index != 1) {    //第一頁跟第二頁不顯示
    return AppBar(
      title: Text(appBarTitles[index]),
      automaticallyImplyLeading: false, //设置没有返回按钮
    );
  }
  //
}
