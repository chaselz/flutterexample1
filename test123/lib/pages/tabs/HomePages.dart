import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomePages extends StatefulWidget {
  @override
  // State<StatefulWidget> createState() {
  //   return _HomeState();
  // }
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<HomePages> {
  List<String> member = [
    "自助优惠",
    "提款银行",
    "资金明细",
    "站内信",
    "账户详情",
    "修改密码",
    "帮助中心",
    "代理加盟",
  ];
  List<String> memberAsset = [
    "coupon",
    "bank",
    "funddetail",
    "message",
    "account",
    "password",
    "help",
    "agent",
  ];
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Container(
          color: Colors.black,
          width: double.infinity,
          height: 800,
          child: Stack(
            alignment: Alignment.center,    //置中
            children: [
              Positioned(
                left: 250.0,
                top: 70,
                child: showImage("assets/images/main.png", 125.0, 125.0),
              ),
              Positioned(
                  top: 100.0,
                  left: 50.0,
                  child: Text.rich(TextSpan(children: [
                    TextSpan(
                      text: "Ｈi,\n",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    TextSpan(
                      text: "Chasel\n",
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                  ]))),
              Positioned(
                  top: 140.0,
                  left: 50.0,
                  child: Container(
                    child: Row(
                      children: [
                        showImage('assets/images/main.png', 15.0, 15.0),
                        Text.rich(TextSpan(children: [
                          TextSpan(
                            text: "至尊水果王",
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        ]))
                      ],
                    ),
                  )),
              Positioned(
                top: 200,
                child: Row(
                  children: [
                    showImage('assets/images/main.png', 50.0, 50.0),
                    Padding(padding: EdgeInsets.symmetric(horizontal: 20.0)),
                    showImage('assets/images/main.png', 50.0, 50.0),
                    Padding(padding: EdgeInsets.symmetric(horizontal: 20.0)),
                    showImage('assets/images/main.png', 50.0, 50.0),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(top: 250.0),
                child: ListView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  //不讓ListView也滾動，因為外層已經有設ScrollView
                  itemCount: member.length,
                  itemExtent: 55,
                  itemBuilder: (BuildContext context, int index) {
                    var name = member[index];
                    return Container(
                      padding: EdgeInsets.all(15.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            child: Row(
                              children: <Widget>[
                                Container(
                                  child: (Image(
                                    image: AssetImage("assets/images/main.png"),
                                  )),
                                ),
                                Container(
                                  padding: EdgeInsets.only(left: 10.0),
                                  child: (Text(
                                    name,
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 16),
                                  )),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            width: 14,
                            height: 14,
                            child: (Image(
                             image: AssetImage(
                                  "assets/images/main.png"),
                            )),
                          ),
                        ],
                      ),
                      // 下边框
                      decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                                  width: 1, color: Color((0xff1f1f1f))))),
                    );
                  },
                ),
              ),
            ],
          )),
    );
  }
}

showImage(path, height, width) {
  //return Image.asset('assets/images/main.png');
  return new Image.asset(
    path, //圖片的路徑
    width: width, //图片控件的宽度
    height: height, //图片控件的高度
    //fit: BoxFit.cover, //告诉引用图片的控件，图像应尽可能小，但覆盖整个控件。
  );
}
