import 'package:flutter/material.dart';
import 'package:test123/Alert.dart';
import 'package:test123/pages/tabs/HomePages.dart';
import 'package:test123/pages/tabs/LoginPages.dart';
import 'package:test123/pages/tabs/registerPage.dart';
import 'package:test123/main.dart';
import 'package:test123/pages/Tabs.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  bool _switchSelected = true; //维护单选开关状态
  bool _checkboxSelected = true; //维护复选框状态
  FocusNode focusNode1 = new FocusNode();
  FocusNode focusNode2 = new FocusNode();
  FocusNode focusNodeLogin = new FocusNode();
  FocusScopeNode focusScopeNode;
  String account = "帳號";
  TextEditingController _unameController = new TextEditingController();
  TextEditingController _pwdController = new TextEditingController();
  GlobalKey _formKey= new GlobalKey<FormState>();

  @override
  void initState() {
    //监听输入改变
    _unameController.addListener(() {
      setState(() {
          //刷新widget狀態
      });
      account = focusNodeLogin.hasFocus?"帳號":"請輸入帳號";
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center, //置中
        children: <Widget>[
          Form(
            key: _formKey, //设置globalKey，用于后面获取FormState
            autovalidate: true, //开启自动校验
            child: Column(
              children: <Widget>[
                Container(
                  child: TextFormField(
                      autofocus: false,
                      focusNode: focusNodeLogin,
                      controller: _unameController,
                      decoration: InputDecoration(
                          //labelText: account,
                          hintText: account,
                          icon: Icon(Icons.person)
                      ),
                      // 校验用户名
                      validator: (v) {
                        return v
                            .trim()
                            .length > 0 ? null : "用户名不能为空";
                      }
                  ),
                ),

                TextFormField(
                    controller: _pwdController,
                    decoration: InputDecoration(
                        labelText: "密码",
                        hintText: "請輸入密碼",
                        icon: Icon(Icons.lock)
                    ),
                    obscureText: true,
                    //校验密码
                    validator: (v) {
                      return v
                          .trim()
                          .length > 5 ? null : "密码不能少于6位";
                    }
                ),
                // 登录按钮
                Padding(
                  padding: const EdgeInsets.only(top: 28.0),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: RaisedButton(
                          padding: EdgeInsets.all(15.0),
                          child: Text("登录"),
                          color: Theme
                              .of(context)
                              .primaryColor,
                          textColor: Colors.white,
                          onPressed: () {
                            //在这里不能通过此方式获取FormState，context不对
                            //print(Form.of(context));

                            // 通过_formKey.currentState 获取FormState后，
                            // 调用validate()方法校验用户名密码是否合法，校验
                            // 通过后再提交数据。
                            if((_formKey.currentState as FormState).validate()){
                              //验证通过提交数据
                              Navigator.push(context, MaterialPageRoute(builder: (context) => Tabs()));
                            }
                          },
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          Column(
            children: <Widget>[
              Theme(
                  data: Theme.of(context).copyWith(
                      hintColor: Colors.red[200], //定义下划线颜色
                      inputDecorationTheme: InputDecorationTheme(
                          labelStyle: TextStyle(color: Colors.grey),//定义label字体样式
                          hintStyle: TextStyle(color: Colors.grey, fontSize: 14.0)//定义提示文本样式
                      )
                  ),
                  child: Column(
                    children: <Widget>[
                      Container(
                        child: TextField(
                            keyboardType: TextInputType.emailAddress,
                            decoration: InputDecoration(
                                labelText: "Email",
                                hintText: "电子邮件地址",
                                prefixIcon: Icon(Icons.email),
                                border: InputBorder.none //隐藏下划线
                            )
                        ),
                        decoration: BoxDecoration(
                          // 下滑线浅灰色，宽度1像素
                            border: Border(bottom: BorderSide(color: Colors.grey[200], width: 1.0))
                        ),
                      ),
                      TextField(
                        decoration: InputDecoration(
                            labelText: "用户名",
                            hintText: "用户名或邮箱",
                            prefixIcon: Icon(Icons.person),
                        ),
                      ),
                      TextField(
                        decoration: InputDecoration(
                            prefixIcon: Icon(Icons.lock),
                            labelText: "密码",
                            hintText: "您的登录密码",
                            hintStyle: TextStyle(color: Colors.grey, fontSize: 13.0)
                        ),
                        obscureText: true,
                      )
                    ],
                  )
              ),
            ],
          ),
          Padding(padding: EdgeInsets.only(top: 50)),
          Container(
              height: 18,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center, //置中
                children: [
                  Text('開關狀態'),
                  Switch(
                    value: _switchSelected, //当前状态
                    onChanged: (value) {
                      //重新构建页面
                      setState(() {
                        _switchSelected = value;
                        printLog(_switchSelected ? '開啟' : '關閉');
                      });
                    },
                  ),
                ],
              )),
          Padding(padding: EdgeInsets.only(top: 10)),
          Container(
              height: 25,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center, //置中
                children: [
                  Text('勾選狀態'),
                  Checkbox(
                    value: _checkboxSelected,
                    activeColor: Colors.red, //选中时的颜色
                    onChanged: (value) {
                      setState(() {
                        _checkboxSelected = value;
                        printLog(_checkboxSelected ? '勾選' : '不勾選');
                      });
                    },
                  ),
                ],
              )),
          RaisedButton(
            child: Text("移动焦点"),
            onPressed: () {
              FocusScope.of(context).requestFocus(focusNode1.hasFocus?focusNode2:focusNode1);   //hasFocus 判斷是否聚焦 requestFocus請求焦點
            },
          ),
          RaisedButton(
            child: Text("隐藏键盘"),
            onPressed: () {
              // 当所有编辑框都失去焦点时键盘就会收起
              focusNode1.unfocus();
              focusNode2.unfocus();
            },
          ),
        ],
      ),
      appBar: AppBar(
        title: Text('登入'),
        automaticallyImplyLeading: false, //设置没有返回按钮
      ),
    );
  }
}

printLog(logString) {
  print(logString);
}

