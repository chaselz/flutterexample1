import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:test123/pages/Tabs.dart';
import 'dart:math' as math;
import 'package:test123/EditShapeImage.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:test123/play_button_icon_icons.dart';
import '../../Arc_progress_main.dart';

class CategoryPages extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _CategorysStae();
  }
}

class _CategorysStae extends State<CategoryPages> {
  List<String> _abs = ['A', 'B', 'S'];

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 375.w,
      height: 1000.h,
      child: Stack(
        children: [
          Scaffold(
            //backgroundColor: Colors.transparent,
            appBar: titleBar(_abs),
            body: Container(),
          ),
          Container(
            width: 375.w,
            height: 700.h,
            child: Stack(
              alignment: Alignment.topCenter,
              children: [
                Positioned(
                  top: 450.h,
                  child: Container(
                    //  color: Colors.red,
                    width: 250.w,
                    height: 180.h,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(1000.0),
                        bottomRight: Radius.circular(1000.0),
                      ),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black26,
                          offset: Offset(0.0, 5.0),
                          blurRadius: 20,
                          //spreadRadius: 40.0,
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  //  color: Colors.red,
                  width: 250.w,
                  height: 630.h,
                  decoration: BoxDecoration(
                    color: Colors.blue,
                    image: DecorationImage(
                        image: AssetImage(
                            'assets/images/shutterstock_739595833-min.jpg'),
                        fit: BoxFit.cover),
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(1000.0),
                      bottomRight: Radius.circular(1000.0),
                    ),
                  ),
                ),
                Positioned(
                    top: 500.h,
                    child: Column(
                      children: [
                        Text(
                          "BabyRage",
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 20.sp,
                          ),
                        ),
                        Padding(padding: EdgeInsets.only(top: 10.h)),
                        Text(
                          "123456789 123456789",
                          style: TextStyle(
                            color: Colors.grey,
                            fontWeight: FontWeight.bold,
                            fontSize: 12.sp,
                          ),
                        ),
                      ],
                    )),
                Positioned(
                    top: 200.h,
                    child: Container(height: 500.h, child: Circular_arc())),
              ],
            ),
          ),
          Positioned(
            top: 710.h,
            child: Container(
              width: 375.w,
              height: 95.h,
              child: Stack(
                clipBehavior: Clip.none,
                children: [
                  Container(
                    height: 95.h,
                    width: 95.w,
                    child: Align(
                      alignment: Alignment(-1.0, 0),
                      child: IconButton(
                        iconSize: 5.h,
                        icon: Icon(PlayButtonIcon.shuffle, color: Colors.black),
                      ),
                    ),
                  ),
                  Positioned(
                    left: 290.w,
                    child: Container(
                      height: 95.h,
                      width: 95.w,
                      child: Align(
                        alignment: Alignment(0.3, 0.3),
                        child: IconButton(
                          iconSize: 35.h,
                          icon: Icon(Icons.list, color: Colors.black),
                        ),
                      ),
                    ),
                  ),
                  Center(
                    child: Container(
                      width: 240.w,
                      child: Stack(
                        overflow: Overflow.visible,
                        alignment: Alignment.center,
                        children: [
                          Positioned(
                            //左
                            top: 30.h,
                            left: 22.5.w,
                            child: Stack(
                              clipBehavior: Clip.none,
                              children: [
                                Container(
                                  height: 70.r,
                                  width: 70.r,
                                  //color: Colors.green,
                                  decoration: BoxDecoration(
                                    //color: Colors.red,
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(1000),
                                        topRight: Radius.circular(1000),
                                        bottomLeft: Radius.circular(1000),
                                        bottomRight: Radius.circular(1000)),
                                    boxShadow: [
                                      BoxShadow(
                                        color:Color.fromARGB(50, 0, 0, 0),
                                        spreadRadius: 5,
                                        blurRadius: 15,
                                        offset: Offset(70.w, 10.h), // changes position of shadow
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  height: 50.h,
                                  width: 90.w,
                                  child: RaisedButton(
                                    onPressed: () {
                                      print('上一首');
                                    },
                                    color: Colors.white,
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(50.0)),
                                  ),
                                ),
                                Container(
                                  height: 50.h,
                                  width: 90.w,
                                  child: Align(
                                    alignment: Alignment(-1.0, 0.5),
                                    child: IconButton(
                                        icon: Icon(
                                          Icons.fast_rewind,
                                          color: Colors.black,
                                          size: 30.r,
                                        ),
                                        onPressed: () {
                                          print('上一首');
                                        }),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Positioned(
                            top: 30.h,
                            left: 130.w,
                            child: Stack(
                              clipBehavior: Clip.none,
                              children: [
                                Container(
                                  height: 50.h,
                                  width: 90.w,
                                  child: RaisedButton(
                                    onPressed: () {
                                      print('下一首');
                                    },
                                    color: Colors.white,
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(50.0)),
                                  ),
                                ),
                                Container(
                                  height: 50.h,
                                  width: 90.w,
                                  child: Align(
                                    alignment: Alignment(1.0, 0.5),
                                    child: IconButton(
                                      icon: Icon(
                                        Icons.fast_forward,
                                        color: Colors.black,
                                        size: 30.h,
                                      ),
                                      onPressed: () {
                                        print('下一首');
                                      },
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          // RawMaterialButton(
                          //   fillColor: Colors.transparent,
                          //   shape:CircleBorder(),
                          //   child:Icon(Icons.play_circle_filled_sharp,size: 95.r),
                          //   elevation: 30.0,
                          //   onPressed: () {
                          //     print("播放");
                          //   },
                          // ),
                          IconButton(
                            iconSize: 95.r,
                            icon: Icon(Icons.circle,color: Colors.black,),
                            onPressed: () {
                              print("播放");
                            },
                          ),
                          Container(
                            child: Align(
                              alignment: Alignment(0.w,2.5.h),
                              child:  IconButton(
                                iconSize: 70.r,
                                icon: Icon(Icons.play_arrow,color: Colors.white),
                                onPressed: () {
                                  print("播放");
                                },
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  titleBar(_abs) {
    return AppBar(
      backgroundColor: Colors.transparent,
      //背景顏色
      elevation: 0,
      //陰影參數
      leading: Builder(
        builder: (BuildContext context) {
          return Container(
            child: Row(
              children: [
                Padding(padding: EdgeInsets.only(left: 0)),
                IconButton(
                  icon: const Icon(Icons.arrow_back_ios, color: Colors.grey),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Tabs()));
                  },
                ),
              ],
            ),
          );
          return IconButton();
        },
      ),
      actions: <Widget>[
        PopupMenuButton(
            onSelected: (val) => print('Selected item is $val'),
            icon: Transform.rotate(
              angle: math.pi / -2, //旋轉圖片
              child: Icon(Icons.bar_chart, color: Colors.grey),
            ),
            itemBuilder: (context) => List.generate(
                _abs.length,
                (index) => PopupMenuItem(
                    value: _abs[index], child: Text(_abs[index]))))
      ],
    );
  }
}

class SimpleShapeBoder extends ShapeBorder {
  @override
  EdgeInsetsGeometry get dimensions => null;

  @override
  Path getInnerPath(Rect rect, {TextDirection textDirection}) {
    return null;
  }

  @override
  Path getOuterPath(Rect rect, {TextDirection textDirection}) {
    return null;
  }

  @override
  void paint(Canvas canvas, Rect rect, {TextDirection textDirection}) {
    var paint = Paint()
      ..color = Colors.white
      ..strokeWidth = 2.0
      ..style = PaintingStyle.stroke
      ..strokeJoin = StrokeJoin.round;
    var w = rect.width;
    var h = rect.height;
    canvas.drawCircle(Offset(0.3 * h, 0.23 * h), 0.12 * h, paint);
    canvas.drawCircle(
        Offset(0.3 * h, 0.23 * h),
        0.06 * h,
        paint
          ..style = PaintingStyle.fill
          ..color = Colors.black);
  }

  @override
  ShapeBorder scale(double t) {
    return null;
  }
}
