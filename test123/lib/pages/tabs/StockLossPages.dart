import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:test123/Alert.dart';
import 'package:test123/pages/tabs/LoginPages.dart';

class StockLossPages extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return _StockLoss();
  }

}

class _StockLoss extends State<StockLossPages> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child:Column(
            mainAxisAlignment: MainAxisAlignment.center,    //置中
            children: [
              RaisedButton(
                child: Text("???"),
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => Alert()));
                  //showAlertDialog(context);
                },
              ),
              RaisedButton(
                child: Text("登入"),
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                  //showAlertDialog(context);
                },
              ),
            ],
          ),
      ),
    );
  }
}


