import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:test123/pages/tabs/LoginPages.dart';
import 'package:test123/pages/tabs/registerPage.dart';

class SettingsPages extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _SettingsStae();
  }
}

class _SettingsStae extends State<SettingsPages> {
  TabController _tabController; //需要定义一个Controller
  final List<Tab> myTabs = <Tab>[
    Tab(text: '登陸'),
    Tab(text: '註冊'),
  ];
  final pages = [Login(), RegisterPage()];

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Container(
        height: 803,
        child: Scaffold(
          appBar: new AppBar(
            automaticallyImplyLeading: false, //设置没有返回按钮
            title: new Text('登入頁面'),
          ),
          body: Column(
            children: [
              Padding(padding: EdgeInsets.only(top: 50.0)),
              Center(
                child: SizedBox(
                  height: 600,
                  width: 400,
                  child: DefaultTabController(
                    length: 2,
                    child: new Scaffold(
                      appBar: new AppBar(
                        backgroundColor: Colors.black, //工具欄顏色
                        toolbarHeight: 60, //工具欄高度
                        bottom: new TabBar(
                          indicatorColor: Colors.white,
                          indicatorWeight: 5.0,
                          tabs: <Widget>[
                            new Tab(child: Text('登入')),
                            new Tab(child: Text('註冊')),
                          ],
                          controller: _tabController,
                        ),
                      ),
                      body: new TabBarView(
                        children: <Widget>[
                          new Center(
                            child: Container(
                              color: Colors.grey,
                              child: Container(
                                width: double.infinity,
                                color: Colors.grey,
                                child: Column(
                                    children: [
                                      TextField(
                                        autofocus: true,
                                        decoration: InputDecoration(
                                            prefixText:"帳號: ",
                                            //labelText: "用户名",
                                            hintText: "用户名或邮箱",
                                            prefixIcon: Icon(Icons.person)
                                        ),
                                      ),
                                    ],
                                ),
                              ),
                            ),
                          ),
                          new Center(
                            child: Text('註冊'),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
