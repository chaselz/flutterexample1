import 'dart:math';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter/material.dart';

class EditShapeImage extends CustomClipper<Path>{

  @override
  Path getClip(Size size){
    Path path = Path();
    path.lineTo(0, size.height.h/1.4);
    path.cubicTo(size.width.w*0.05, 660.h,
        size.width.w*0.95, 660.h,
        size.width.w, size.height.h/1.4);
    path.lineTo(size.width.w, 0);
    path.lineTo(0,0);
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    // TODO: implement shouldReclip
    return false;
  }
}


